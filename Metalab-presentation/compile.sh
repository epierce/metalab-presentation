# quit if one command fails
set -e

METALAB_BIB=../metalabbib/metalab-papers.bib
if [ ! -f $METALAB_BIB ] ; then git submodule init; fi
if [ ! -f $METALAB_BIB ] ; then echo metalab bibliography file not found in submodule, document cannot be compiled; exit; fi

# (fr) generating presentation-metalab.pdf
latexmk presentation-metalab.tex

# (eng) generating presentation-metalab-eng.pdf
latexmk presentation-metalab-eng.tex


# # (eng) generating presentation-metalab-eng.pdf
# xelatex presentation-metalab-eng.tex
# # for i in Met  Talk Interns; do biber $i; done
# biber presentation-metalab-eng
# xelatex presentation-metalab-eng.tex
# xelatex presentation-metalab-eng.tex
