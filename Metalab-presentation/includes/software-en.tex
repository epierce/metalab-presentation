\section{SATIE}
\label{sec:satie}

\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/SATIE/CMYK/Satie_CMYK.eps}
\end{wrapfigure}
The Spatial Audio Toolkit for Immersive Environments (\textit{SATIE}) is an audio spatialization engine capable of live rendering of dense (several hundred sound sources) and dynamic (individual sound source creation, removal and control) audio scenes. \textit{SATIE} uses the SuperCollider language and the OSC protocol, allowing control from external tools such as 3D engines and programming languages. 

SATIE facilitates the prototyping of innovative immersive sound spaces. Its features include:
\begin{itemize}
\item Parameterizable spatializer for specific sound devices, such as those of the SAT (mini domes, dome, cubes, stereo)
\item Capable of live and simultaneous rendering for multiple types of speaker systems
\item OSC control from 3D engines or other
\item Ability to write audio source extensions, audio effects, parameter settings and spatializers
\item Playback of audio files
\item Audio synthesis
\item Tested under Linux and OSX, should work under Windows
\item Distributed as a quark SuperCollider
\end{itemize}

Development of \textit{SATIE} began in 2015. It is distributed under the GPL and can be downloaded here: \url{https://gitlab.com/sat-metalab/satie}.

Demonstrations:
\begin{itemize}
\item Using SATIE with Mozilla Hubs: \url{https://vimeo.com/549433773}
\item Using SATIE with two spatialization devices, the Audiodice and the Satosphere. (Prototyping a) Immersive Navigation Experience in the Montreal Symphony Orchestra: \url{https://vimeo.com/519940468}
\item Generation of a 3D animation with Blender and SATIE: \url{https://vimeo.com/398346954}
\item Real time navigation in a 3D environment (EiS + SATIE) : \url{https://vimeo.com/265607999}
\end{itemize}

\section{vaRays}
\label{sec:varays}

\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/vaRays/CMYK/Varays_C.eps}
\end{wrapfigure}
\textit{VaRays}, for \textit{virtual audio rays}, is a tool allowing the calculation in real time of impulse responses of a virtual environment. It takes into account the geometry of the environment, the position of the listener and that of the sound objects. This tool is in its early stages of development, having started in 2018. It is currently used in conjunction with \textit{EiS}~(\ref{sec:eis}), for geometry, and \textit{SATIE}~(\ref{sec:satie}), for impulse response based reverberation rendering.

\textit{VaRays} is distributed under LGPL license and can be downloaded here:  \url{https://gitlab.com/sat-metalab/vaRays}.

Demonstrations:
\begin{itemize}
\item Live sound ray launching, and Auralization, of 3D audio scene with vaRays -- Presentation at the SMC conference: \url{https://vimeo.com/574450746}
\item Navigation in the model and sounds of Bretez's project (Paris in the 18th century) : \url{https://vimeo.com/507255065}
\item Live acoustic simulation with vaRayz : \url{https://vimeo.com/manage/videos/484122810}
\end{itemize}

\section{Édition in Situ}
\label{sec:eis}

\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/EiS/CMYK/Edition_in_situ_C.eps}
\end{wrapfigure}
\textit{Edition in Situ (EiS)} is an immersive environment creation platform designed to allow the prototyping of 3D scenography. The hypothesis behind the development of this platform is that by allowing creation in an immersive context, the user will have the ability to experiment in more detail and more quickly, leading to a shorter creation cycle and original content. It integrates existing tools (\textit{Splash}~(\ref{sec:splash}), \textit{SATIE}~(\ref{sec:satie})) as well as new software to allow the user to manipulate the 3D scene using devices adapted to virtual reality interaction (currently, HTC Vive controllers\footnote{\url{https://www.vive.com/us/}}).

Its features includes:
\begin{itemize}
\item Real-time rendering for specific immersive space (format \textit{cubemap}\footnote{\url{https://en.wikipedia.org/wiki/Cube_mapping}})
\item Navigation in the 3D space;
\item Importing content (objects, textures, sounds);
\item Transformation of objects (translation, rotation, scale) ;
\item Animation ;
\item Sound spatialization with \textit{SATIE}.
\end{itemize}

The development of \textit{EiS} started in 2015. It is distributed under GPL license and can be downloaded here: \url{https://gitlab.com/sat-metalab/EditionInSitu}.

Demonstrations:
\begin{itemize}
\item Basic operations to create an immersive installation from scratch with EiS : \url{https://vimeo.com/339326874}
\item Experimentation with EiS - History of SAT : \url{https://sat.qc.ca/fr/nouvelles/presentation-experimentale-de-la-plateforme-eis}
\item Fulldome animation drafting : \url{https://vimeo.com/226298383}

\end{itemize}

\newpage
\section{Switcher}
\label{sec:switcher}

\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/Switcher/eps/Switcher-color.eps}
\end{wrapfigure}
\textit{Switcher} is a tool for creating collaborative experiences over the Internet. It is capable of integrating multiple technologies, such as network protocols and other applications that operate in real time. It is most often used for its ability to simultaneously transmit multi-channel audio, video and low latency data streams over the Internet.

\textit{Switcher} is actually more generic and relies on an architecture of services (called quiddities). Each quiddity can be created and deleted dynamically, but can also be controlled using property read/write or method invocation. Quiddity monitoring is done using queries to its information tree, possibly through serialized string in the JSON format\footnote{\url{https://www.json.org/json-en.html}}. \textit{Switcher} provides introspection mechanisms allowing it to be used as a library for developing higher-level applications (in C++, NodeJS\footnote{\url{https://nodejs.org/en/}} or Python). \textit{Switcher} also allows to save and download the state of the active quiddities at a given time. 

Most quiddities produce and/or consume data streams using the \textit{shmdata} library (see section~(\ref{sec:shmdata})), thus allowing the transfer of streams between other quiddities and/or applications. For example, a webcam service (v4l2src service) provides a video stream that can simultaneously connect to several other services, such as a window display (glfwin service), a file recorder (avrec service), a low latency network transmitter (SIP service), but also simultaneously to other software compatible with shmdata.

The development of \textit{Switcher} started in 2012. It is distributed under GPL and LGPL licenses. It can be downloaded here: \url{https://gitlab.com/sat-metalab/switcher}

\textit{Switcher} is the basis for more specific telepresence systems, also developed at SAT:
\begin{itemize}
\item Scenic, a web-based graphical interface to switcher~: \url{https://sat-mtl.gitlab.io/telepresence/scenic}
\item Bibliolab, networked connection of Montreal libraries~: \url{https://sat.qc.ca/en/scenic-telepresence}
\item Scènes ouvertes, a network of about twenty Quebec venues: \url{https://sat.qc.ca/en/scenic-telepresence}
\end{itemize}

Demonstrations:
\begin{itemize}
\item The Bluff theater project: \url{https://vimeo.com/530865445}
\item Streaming test from a fisheye camera to two fulldomes : \url{https://vimeo.com/266687560}
\item Waterfall Music during Linux Audio Conference 2013:  \url{https://vimeo.com/304831248}
\item The Drawing Space - Siggraph 2014: \url{https://vimeo.com/304826675}
\end{itemize}

\section{Shmdata}
\label{sec:shmdata}
\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/Shmdata/eps/Shmdata-color.eps}
\end{wrapfigure}
\textit{Shmdata} is a tool allowing zero-copy transmission between programs running on the same computer. \textit{Shmdata} works without a server: an application emitting a data stream (a shmdata) must provide a path (e.g. \textit{/tmp/myshm}) which will be used by the programs wishing to read the stream. \textit{Shmdata} allows the transmission of data between programs with very low latency.

The communication paradigm of \textit{shmdata} is \textit{1-to-multiple}, i.e. the writer of a stream makes the images of the stream available to possibly several followers (readers). During a transmission, the subscribers and the writer are able to leave the transmission and return along the way, without having to restart the communication. A shmdata can be resized during transmission. In addition, shmdata are typed, using a string published by the writer at each (re)connection. 

\textit{Shmdata} can be used by applications written in C++ or Python 3; a GStreamer\footnote{\url{https://gstreamer.freedesktop.org/}} element is also provided. 

The development of \textit{shmdata} started in 2012. It is distributed under LGPL license. It can be downloaded here: \url{https://gitlab.com/sat-metalab/shmdata}

\section{Splash}
\label{sec:splash}
\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/Splash/CMYK/Splash_CMYK.eps}
\end{wrapfigure}
\textit{Splash} is a real-time projection mapping engine capable of handling a theoretically unlimited number of projectors. It has been designed as a generic and modular tool, allowing it to be used for any projection surface geometry. It is used in production at SAT in the Satosphere, which is equipped with eight HD projectors. \textit{Splash} is compatible with \textit{shmdata}~(\ref{sec:shmdata}) for the reception of the video stream to be broadcast and for the communication in real time of 3D model from Blender\footnote{\url{https://blender.org}}.

Characteristics:
\begin{itemize}
\item number of projectors unlimited (except by the power of the computer and the number of video outputs)
\item support of numerous video codecs, including Hap\footnote{\url{https://github.com/Vidvox/hap}}
\item support for Video4Linux2 compatible capture cards
\item semi-automatic calibration of projectors (the user must have a 3D model of the projection surface)
\item projection deformation (\textit{warping})
\item automatic calculation of the overlapping zones (blending)
\item automatic colorimetric calibration of the projectors
\item Python 3 compatible
\end{itemize}

The development of \textit{Splash} started in 2014. It is distributed under GPL license and can be downloaded here~: \url{https://gitlab.com/sat-metalab/splash}.

Demontrations:
\begin{itemize}
\item Remote rendering of a multi-output OpenGL software : \url{https://vimeo.com/456217092}
\item Calibration of projection mapping with Splash : \url{https://vimeo.com/314519230}
\item Projection on mobile objects : \url{https://vimeo.com/268028595}
\end{itemize}

\section{Calimiro}
\label{sec:calimiro}
\begin{wrapfigure}[5]{l}[10pt]{1.5cm}
  \includegraphics[width=\linewidth]{../metalabcomm/logos/Calimiro/CMYK/Calimiro_N.eps}
\end{wrapfigure}
\textit{Calimiro} is a tool for the calibration of immersive environment with structured light. It takes into account multi-projector environments. This tool is in its early stages of development, having started in 2018. It is used in conjunction with \textit{Splash} (see section~\ref{sec:splash}).

This software library is distributed under LGPL license and can be downloaded here~: \url{https://gitlab.com/sat-metalab/calimiro}

Demonstrations:
\begin{itemize}
\item Automated generation of UV coordinates : \url{https://vimeo.com/544570900}
\item Automatic calibration of video projection with Calimiro and Splash : \url{https://vimeo.com/499279721}
\item Automatic projection calibration: \url{https://vimeo.com/329425110}
\end{itemize}

\section{LivePose}
\label{sec:livepose}

\textit{LivePose} is a command line tool for capturing human poses and movements in real time, using several popular computer vision models. Wishing to focus on the sustainability of its uses, we chose to reimplement these models based on the deep learning module of OpenCV\footnote{\url{https://opencv.org}} rather than using the implementations made in the research work of the different laboratories. As OpenCV is available on many hardware platforms, and benefits from the support of a very strong community, we expect less maintenance of each model.

Currently, it is possible to choose between the pose estimation algorithms OpenPose\footnote{\url{https://github.com/CMU-Perceptual-Computing-Lab/openpose}} and PoseNet\footnote{\url{https://github.com/google-coral/project-posenet}}, depending on whether one wishes to optimize for accuracy or speed. \textit{LivePose} also uses custom filters to extract additional information from the poses, such as gesture detection: hand salutes, jumps, etc. 

Another specificity of \textit{LivePose} is to be able to run on embedded hardware. In particular, \textit{LivePose} works on NVIDIA Jetson. One of the objectives of the tool is to allow the deployment of a network of cameras to allow the capture of poses of many users on a large area.

\textit{LivePose} is designed to be used in conjunction with 3D engines such as Mozilla Hubs, or immersive environments such as the Satosphere; in this case, filters can trigger actions based on the gestures of the participants, thus constituting a user interface that is based on movements and gestures, freeing itself from manual input.

This software library is distributed under LGPL license and can be downloaded here: \url{https://gitlab.com/sat-metalab/livepose}.

  
