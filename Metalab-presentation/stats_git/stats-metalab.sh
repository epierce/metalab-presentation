#/bin/bash

set -e

# ALL_METALAB_URLS="\
#         sat-metalab/EditionInSitu \
#         sat-metalab/InSituImmersiveAuthoring \
#         sat-metalab/SATIE-sc \
#         sat-metalab/blobserver \
#         sat-metalab/calimiro \
#         sat-metalab/documentations/satie \
#         sat-metalab/documentations/shmdata \
#         sat-metalab/documentations/splash \
#         sat-metalab/ndi2shmdata \
#         sat-metalab/platoreso \
#         sat-metalab/poire \
#         sat-metalab/padweb \
#         sat-metalab/pdsheefa \
#         sat-metalab/spatosc \
#         sat-metalab/scenic-legacy \
#         sat-metalab/spinic \
#         sat-metalab/spinframework \
#         sat-metalab/posturevision \
#         sat-metalab/pysatie \
#         sat-metalab/satie \
#         sat-metalab/satie4blender\
#         sat-metalab/satie4unity \
#         sat-metalab/satie4unityExample \
#         sat-metalab/shmdata \
#         sat-metalab/splash \
#         sat-metalab/livepose \
#         sat-metalab/switcher \
#         sat-metalab/tools/ambir \
#         sat-metalab/varays \
#         sat-mtl/satellite/hubs-custom-client \
#         sat-mtl/satellite/hubs-injection-server \
#         sat-mtl/telepresence/node-switcher \
#         sat-mtl/telepresence/scenic \
#         sat-mtl/telepresence/scenic-core \
#         sat-mtl/telepresence/sip-server \
#         sat-mtl/telepresence/ui-components \
# "

METALAB_URLS="\
        sat-metalab/EditionInSitu \
        sat-metalab/calimiro \
        sat-metalab/documentations/satie \
        sat-metalab/documentations/shmdata \
        sat-metalab/documentations/splash \
        sat-metalab/ndi2shmdata \
        sat-metalab/poire \
        sat-metalab/pysatie \
        sat-metalab/satie \
        sat-metalab/satie4blender\
        sat-metalab/satie4unity \
        sat-metalab/satie4unityExample \
        sat-metalab/shmdata \
        sat-metalab/splash \
        sat-metalab/livepose \
        sat-metalab/switcher \
        sat-metalab/tools/ambir \
        sat-metalab/varays \
"

function url_to_folder {
    echo $i | sed 's/\//-/g'
}

for i in ${METALAB_URLS}
do
    FOLDER=$(url_to_folder $i) 
    METALAB_SOFTWARE=$(echo ${METALAB_SOFTWARE} ${FOLDER})
done
                       
# stats will end in last year + 1:
YEARS="2011 2012 2013 2014 2015 2016 2017 2018 2019 2020 2021" 
START_DATE="04-01" # month-day
END_DATE="03-31"
STAT_FILE_TOTAL="stat_total.csv"
STAT_FILE_COMMITS="stat_commits.csv"
STAT_FILE_CONTRIBUTORS="stat_active_contributors.csv"
STAT_FILE_TAGS="stat_tags.csv"

function get_repo {
    echo get or update repo $1
    FOLDER=$(url_to_folder $1) 
    if [ ! -d ${FOLDER} ] ; then git clone git@gitlab.com:$1.git ${FOLDER}> /dev/null; fi
    cd ${FOLDER}
    git fetch > /dev/null
    git pull origin > /dev/null
    cd ..
}

function filter_authors {
    while read stdinput
    do
    echo $stdinput | \
        sed 's/Arnaud$/Arnaud Grosjean/'| \
        sed 's/Alexandre Quessy (posture08)/Alexandre Quessy/'| \
        sed 's/alexandre$/Alexandre Quessy/'| \
        sed 's/Elisabeth$/Elisabeth Fagnan/'| \
        sed 's/Fran]ois Ubald Brien/François Ubald Brien/'| \
        sed 's/Francois/François/' | \
        sed 's/Frederic Lestage/Frédéric Lestage/'| \
        sed 's/Hantz V$/Hantz-Carly F. Vius/' | \
        sed 's/Jeremie Soria/Jérémie Soria/' | \
        sed 's/Marie$/Marie-Eve Dumas/' | \
        sed 's/Marie-Eve$/Marie-Eve Dumas/' | \
        sed 's/Michal Seta$/Michał Seta/'| \
        sed 's/Nina$/Nicolas Bouillot/' | \
        sed 's/Vampolka$/Emile Ouellet-Delorme/' | \
        sed 's/Zack Konate/Zack Settel/'| \
        sed 's/^Chesnel$/Pierre-Antoine Chesnel/'| \
        sed 's/^chesnel$/Pierre-Antoine Chesnel/'| \
        sed 's/^io$/Io Andes Daza-Dillon/'| \
        sed 's/^zk$/Zack Settel/'| \
        sed 's/^evsc$/Eva Schindling/'| \
        sed 's/eodelorme$/Emile Ouellet-Delorme/' | \
        sed 's/eviau/Edith Viau/' | \
        sed 's/flecavalier/Francis Lecavalier/'| \
        sed 's/fphilippe/François Philippe/'| \
        sed 's/lbouchard/Louis Bouchard/'| \
        sed 's/lwi/Louis Bouchard/'| \
        sed 's/francoisph/François Philippe/'| \
        sed 's/jwantz/Julien Wantz/'| \
        sed 's/nicolas$/Nicolas Bouillot/' | \
        sed 's/patrickdupuis$/Patrick Dupuis/'| \
        sed 's/pchevry/Philippe Chevry/'| \
        sed 's/ssdia/serigne saliou dia/' | \
        sed 's/ubald$/François Ubald Brien/' | \
        sed 's/vlaurent$/Valentin Laurent/' | \
        sed 's/vlnk$/Valentin Laurent/' | \
        sed 's/zacco$/Zack Settel/'| \
        sed 's/zackie$/Zack Settel/'| \
        sed 's/zack$/Zack Settel/'| \
        sed 's/ZACK$/Zack Settel/'| \
        sed 's/zack settel/Zack Settel/'| \
        sed '/4d3d3d3$/d' | \
        sed '/OpSocket$/d' | \
        sed '/Metalab$/d' | \
        sed '/aquakhoria$/d' | \
        sed '/immersif$/d' | \
        sed '/metalab$/d' | \
        sed '/posture$/d'
    done
}

function total_stats {
    cd $1
    COMMIT_NUM=$(git rev-list --no-merges --all --count)
    CONTRIB_FILE=../contrib_$1_total.data
    git log --no-merges --all  --format='%aN' | filter_authors | sort | uniq -c > ${CONTRIB_FILE}
    CONTRIB_NUM=$(cat ${CONTRIB_FILE} | wc -l)
    TAGS_NUM=$(git log --no-walk --tags --oneline | wc -l)
    echo $1,${COMMIT_NUM},${CONTRIB_NUM},${TAGS_NUM}
    cd ..
}

function commit_stats {
    STATS=$1
    cd $1
    for y in ${YEARS}
    do
        y_next=$((y+1))
        # number of commit across all branches during the period
        # NOTE: rev-list and --all take all commits, from all branches, even if not merged or squashed in the main history
        COMMIT_NUM=$(git rev-list --no-merges --all --count --since ${y}-${START_DATE} --until ${y_next}-${END_DATE})
        STATS=$(echo ${STATS},${COMMIT_NUM})
    done
    echo $STATS
    cd ..
}

function active_contributors_stats {
    cd $1
    STATS=$1
    for y in ${YEARS}
    do
        y_next=$((y+1))
        CONTRIB_FILE=../contrib_$1_${y}-${START_DATE}_${y_next}-${END_DATE}.data
        # number of contributors
        git log --no-merges --all --since ${y}-${START_DATE} --until ${y_next}-${END_DATE} --format='%aN' \
	| filter_authors | sort | uniq -c > ${CONTRIB_FILE}   
        # save contributors into a file
        CONTRIB_NUM=$(cat ${CONTRIB_FILE} | wc -l) 
        STATS=$(echo ${STATS},${CONTRIB_NUM})
    done
    echo $STATS
    cd ..
}

function tag_stats {
    cd $1
    STATS=$1
    for y in ${YEARS}
    do
        y_next=$((y+1))
        TAGS_NUM=$(git log --no-walk --tags --oneline --since ${y}-${START_DATE} --until ${y_next}-${END_DATE} | wc -l)
        STATS=$(echo ${STATS},${TAGS_NUM})
    done
    echo $STATS
    cd ..
}


# Get all repos
# =============
for i in ${METALAB_URLS}
do
    get_repo $i
done

# Total stats
# ===========================

echo repo,commits,contributors,versions > ${STAT_FILE_TOTAL}
# running the stat for each repo
for i in ${METALAB_SOFTWARE}
do
    echo total stats for $i 
    total_stats $i >> ${STAT_FILE_TOTAL}
done


# writer headers for commits, contributors and tags statitics files
# =================================================================
HEADER_STATS="repo"
for y in ${YEARS}
do
    y_next=$((y+1))
    HEADER_STATS=$(echo ${HEADER_STATS},${y}-${y_next})
done
echo $HEADER_STATS > ${STAT_FILE_COMMITS}
echo $HEADER_STATS > ${STAT_FILE_CONTRIBUTORS}
echo $HEADER_STATS > ${STAT_FILE_TAGS}

# number of commits per year
# ==========================
for i in ${METALAB_SOFTWARE}
do
    echo commits stats for $i 
    commit_stats $i >> ${STAT_FILE_COMMITS}
done

# active contributor per year
# ===========================
for i in ${METALAB_SOFTWARE}
do
    echo active contributors stats for $i 
    active_contributors_stats $i >> ${STAT_FILE_CONTRIBUTORS}
done

# number of tags per year
# =======================
for i in ${METALAB_SOFTWARE}
do
    echo tag stats for $i 
    tag_stats $i >> ${STAT_FILE_TAGS}
done

# compact years from 20xx-20yy to xx/yy
# ======================================

for i in ${STAT_FILE_COMMITS} ${STAT_FILE_CONTRIBUTORS} ${STAT_FILE_TAGS} ${STAT_FILE_TOTAL}
do
    # compact years from 20xx-20yy to xx/yy
    sed -i 's/20\(..\)-20\(..\)/\1\/\2/g' $i
    # compact repo names
    sed -i 's/sat-metalab-//g' $i
    sed -i 's/documentations-/doc-/g' $i
    sed -i 's/sat-mtl-satellite-//g' $i
    sed -i 's/sat-mtl-telepresence-//g' $i
done

# convert csv to readable txt files
# =================================

for i in ${STAT_FILE_COMMITS} ${STAT_FILE_CONTRIBUTORS} ${STAT_FILE_TAGS} ${STAT_FILE_TOTAL}
do
    ./csv_to_txt.py -f $i
done
# 
# remove repos
# ============
# for i in ${METALAB_SOFTWARE}
# do
#     if [ ! -d $i ]
#     then
#         rm -rf $i
#     fi
# done
